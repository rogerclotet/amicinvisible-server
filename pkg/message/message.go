package message

import "fmt"

// New creates a new Message with the given type
func New(typeName string) Message {
	return Message{
		Type: typeName,
		Args: make(map[string]string),
	}
}

// A Message represents a message to be sent and received
// via websocket
type Message struct {
	Type string            `json:"type"`
	Args map[string]string `json:"args"`
}

// WithArgument returns a message with the same information
// with the defined argument value
func (m Message) WithArgument(name string, value string) Message {
	m.Args[name] = value
	return m
}

// GetArgument returns an argument value if defined
func (m Message) GetArgument(name string) (string, error) {
	arg, ok := m.Args[name]
	if !ok {
		return "", fmt.Errorf("missing argument %s", name)
	}

	return arg, nil
}

func (m Message) String() string {
	return fmt.Sprintf("{%s: %+v}", m.Type, m.Args)
}
