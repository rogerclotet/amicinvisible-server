package message_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/message"
)

func TestMessage(t *testing.T) {
	m := message.Message{
		Type: "test_type",
		Args: map[string]string{
			"first":  "first_test",
			"second": "second_test",
		},
	}

	assert.Equal(t, "test_type", m.Type)

	first, err := m.GetArgument("first")
	assert.NoError(t, err)
	assert.Equal(t, "first_test", first)

	second, err := m.GetArgument("second")
	assert.NoError(t, err)
	assert.Equal(t, "second_test", second)

	_, err = m.GetArgument("third")
	assert.Error(t, err)
}

func TestNewWithArgument(t *testing.T) {
	m := message.
		New("test_type").
		WithArgument("first", "first_test").
		WithArgument("second", "second_test")

	assert.Equal(t, "test_type", m.Type)

	first, err := m.GetArgument("first")
	assert.NoError(t, err)
	assert.Equal(t, "first_test", first)

	second, err := m.GetArgument("second")
	assert.NoError(t, err)
	assert.Equal(t, "second_test", second)

	_, err = m.GetArgument("third")
	assert.Error(t, err)
}
