package websocket

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/rogerclotet/amicinvisible-server/pkg/manager"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/message"
	"golang.org/x/net/websocket"
)

// A Handler handles websocket connections and routes them to the manager
type Handler interface {
	Handler() http.Handler
}

// NewHandler creates a new Handler
func NewHandler(m manager.Manager) Handler {
	return &handler{
		manager: m,
	}
}

type handler struct {
	manager manager.Manager
}

func (h *handler) Handler() http.Handler {
	return websocket.Handler(func(ws *websocket.Conn) {
		defer func() {
			err := ws.Close()
			if err != nil {
				log.Printf("could not close websocket: %s", err)
			}
		}()

		log.Print("client connected")

		connectedUserName := ""

		for {
			var msg message.Message
			err := websocket.JSON.Receive(ws, &msg)
			if err != nil {
				if err.Error() == "EOF" {
					if connectedUserName != "" {
						err = h.manager.Disconnect(connectedUserName)
						if err != nil {
							log.Printf("could not disconnect automatically: %s", err)
						}
					}
					log.Printf("connection closed")
					return
				}

				log.Printf("error receiving message: %s", err)
				sendErr := websocket.JSON.Send(ws, message.New("error").WithArgument("message", fmt.Sprintf("error receiving message: %s", err)))
				if sendErr != nil {
					log.Printf("could not send error message: %s", err)
				}

				continue
			}

			log.Printf("received message: %v", msg)

			if msg.Type == "connect" {
				var userID, userName, picture string
				userName, err = msg.GetArgument("user_name")
				if err != nil {
					log.Printf("missing user_name in connect")

					continue
				}

				picture, err = msg.GetArgument("picture")
				if err != nil {
					log.Printf("missing picture in connect")

					continue
				}

				userID, err = msg.GetArgument("user_id")
				if err == nil {
					c, groups, connectErr := h.manager.Connect(userID, userName, picture)
					if connectErr != nil {
						log.Printf("could not connect to manager: %v", connectErr)
						sendErr := websocket.JSON.Send(ws, message.New("error").WithArgument("message", fmt.Sprintf("could not connect to manager: %v", connectErr)))
						if sendErr != nil {
							log.Printf("could not send error message: %s", err)
						}

						continue
					}

					connectedUserName = userID

					go sendResponses(c, ws)

					groupsData, marshalErr := json.Marshal(groups)
					if marshalErr != nil {
						log.Printf("could not encode groups data: %s", marshalErr)
						sendErr := websocket.JSON.Send(ws, message.New("error").WithArgument("message", fmt.Sprintf("could not connect to manager: %s", marshalErr)))
						if sendErr != nil {
							log.Printf("could not send error message: %s", err)
						}
					} else {
						err = websocket.JSON.Send(ws, message.New("connected").WithArgument("groups", string(groupsData)))
						if err != nil {
							log.Printf("could not send connected message: %s", err)
						}
					}
				}
			} else {
				err = h.manager.Process(msg)
			}

			if err != nil {
				log.Printf("error processing %s message: %s", msg.Type, err)

				sendErr := websocket.JSON.Send(ws, message.New("error").WithArgument("message", err.Error()))
				if sendErr != nil {
					log.Printf("error sending error message: %s", sendErr)
				}
			}
		}
	})
}

func sendResponses(c manager.Connection, ws *websocket.Conn) {
	resp := c.Responses()

	for msg := range resp {
		log.Println("sending message", msg)

		err := websocket.JSON.Send(ws, msg)
		if err != nil {
			log.Printf("error sending message to websocket: %s", err)
		}
	}
}
