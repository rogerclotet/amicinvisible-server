package websocket_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"sync"
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/manager"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/message"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/user"
	ws "gitlab.com/rogerclotet/amicinvisible-server/pkg/websocket"
	"golang.org/x/net/websocket"
)

func TestConnect(t *testing.T) {
	m := testManager(t)
	h := ws.NewHandler(m)

	mux := http.NewServeMux()
	mux.Handle("/ws", h.Handler())

	s := httptest.NewServer(mux)
	defer s.Close()

	wsConn, err := dialWebsocket(s.URL)
	assert.NoError(t, err)

	r := setUpReceiver(t, wsConn)
	wgCreated, msgCreated := r.expectResponse("group_created")

	ownerID := "owner"
	_, _ = r.expectResponse("connected") // ignore the first connection response

	err = websocket.JSON.Send(wsConn, message.New("connect").WithArgument("user_id", ownerID).WithArgument("user_name", "test_name").WithArgument("picture", ""))
	assert.NoError(t, err)

	err = websocket.JSON.Send(wsConn, message.New("create_group").WithArgument("user_id", ownerID).WithArgument("description", "").WithArgument("group_name", "group"))
	assert.NoError(t, err)

	wgCreated.Wait()
	groupStr := msgCreated.Args["group"]
	groupData := map[string]interface{}{}
	err = json.Unmarshal([]byte(groupStr), &groupData)
	assert.NoError(t, err)

	groupID, ok := groupData["id"].(string)
	assert.True(t, ok)

	ids := []string{"foo", "bar", "baz"}
	for _, id := range ids {
		_, _, err = m.Connect(id, "test_name", "")
		assert.NoError(t, err)
		err = m.Disconnect(id)
		assert.NoError(t, err)

		err = m.JoinGroup(id, groupID)
		assert.NoError(t, err)
	}

	err = m.Disconnect(ownerID)
	assert.NoError(t, err)

	wg, msg := r.expectResponse("connected")

	err = websocket.JSON.Send(wsConn, message.New("connect").WithArgument("user_id", ownerID).WithArgument("user_name", "test_name").WithArgument("picture", ""))
	assert.NoError(t, err)

	wg.Wait()

	groupsStr := msg.Args["groups"]
	var groups []map[string]interface{}
	err = json.Unmarshal([]byte(groupsStr), &groups)
	assert.NoError(t, err)

	assert.Len(t, groups, 1)
	assert.Len(t, groups[0]["members"], 4)
}

func TestJoinAndLeaveGroup(t *testing.T) {
	m := testManager(t)
	h := ws.NewHandler(m)

	mux := http.NewServeMux()
	mux.Handle("/ws", h.Handler())

	s := httptest.NewServer(mux)
	defer s.Close()

	wsConnOwner, err := dialWebsocket(s.URL)
	assert.NoError(t, err)
	wsConnJoiner, err := dialWebsocket(s.URL)
	assert.NoError(t, err)

	ownerReceiver := setUpReceiver(t, wsConnOwner)
	joinerReceiver := setUpReceiver(t, wsConnJoiner)

	wgCreated, createdMsg := ownerReceiver.expectResponse("group_created")
	wgUpdatedOwner, updatedOwnerMsg := ownerReceiver.expectResponse("group_updated")
	wgJoined, joinedMsg := joinerReceiver.expectResponse("group_joined")
	wgUpdatedJoiner, updatedJoinerMsg := joinerReceiver.expectResponse("group_updated")

	ownerName := "owner_name"
	err = websocket.JSON.Send(wsConnOwner, message.New("connect").WithArgument("user_id", ownerName).WithArgument("user_name", "test_name").WithArgument("picture", ""))
	assert.NoError(t, err)
	joinerName := "joiner_name"
	err = websocket.JSON.Send(wsConnJoiner, message.New("connect").WithArgument("user_id", joinerName).WithArgument("user_name", "test_name").WithArgument("picture", ""))
	assert.NoError(t, err)

	err = websocket.JSON.Send(wsConnOwner, message.New("create_group").WithArgument("user_id", ownerName).WithArgument("group_name", "test_group").WithArgument("description", ""))
	assert.NoError(t, err)

	wgCreated.Wait()
	groupStr, err := createdMsg.GetArgument("group")
	assert.NoError(t, err)

	groupData := map[string]interface{}{}
	err = json.Unmarshal([]byte(groupStr), &groupData)
	assert.NoError(t, err)

	groupID, ok := groupData["id"].(string)
	assert.True(t, ok)

	err = websocket.JSON.Send(wsConnJoiner, message.New("join_group").WithArgument("group_id", groupID).WithArgument("user_id", joinerName))
	assert.NoError(t, err)

	wgJoined.Wait()
	assert.Equal(t, message.New("group_joined"), *joinedMsg)

	wgUpdatedOwner.Wait()
	ownerGroup := updatedOwnerMsg.Args["group"]

	wgUpdatedJoiner.Wait()
	joinerGroup := updatedJoinerMsg.Args["group"]

	assert.Equal(t, ownerGroup, joinerGroup)

	var groupResponseData map[string]interface{}
	err = json.NewDecoder(strings.NewReader(ownerGroup)).Decode(&groupResponseData)
	assert.NoError(t, err)

	assert.Len(t, groupResponseData["members"], 2)

	wgLeft, msgLeft := joinerReceiver.expectResponse("group_left")

	err = websocket.JSON.Send(wsConnJoiner, message.New("leave_group").WithArgument("user_id", joinerName).WithArgument("group_id", groupID))
	assert.NoError(t, err)

	wgLeft.Wait()

	var leftGroupData map[string]interface{}
	groupStr = msgLeft.Args["group"]
	err = json.Unmarshal([]byte(groupStr), &leftGroupData)
	assert.NoError(t, err)

	assert.Equal(t, groupID, groupData["id"].(string))
}

func TestStart(t *testing.T) {
	m := testManager(t)
	h := ws.NewHandler(m)

	mux := http.NewServeMux()
	mux.Handle("/ws", h.Handler())

	s := httptest.NewServer(mux)
	defer s.Close()

	wsConnOwner, err := dialWebsocket(s.URL)
	assert.NoError(t, err)
	wsConnJoiner, err := dialWebsocket(s.URL)
	assert.NoError(t, err)

	ownerReceiver := setUpReceiver(t, wsConnOwner)
	joinerReceiver := setUpReceiver(t, wsConnJoiner)

	wgCreated, createdMsg := ownerReceiver.expectResponse("group_created")

	ownerName := "owner_name"
	err = websocket.JSON.Send(wsConnOwner, message.New("connect").WithArgument("user_id", ownerName).WithArgument("user_name", "test_name").WithArgument("picture", ""))
	assert.NoError(t, err)
	joinerName := "joiner_name"
	err = websocket.JSON.Send(wsConnJoiner, message.New("connect").WithArgument("user_id", joinerName).WithArgument("user_name", "test_name").WithArgument("picture", ""))
	assert.NoError(t, err)

	err = websocket.JSON.Send(wsConnOwner, message.New("create_group").WithArgument("user_id", ownerName).WithArgument("group_name", "test_group").WithArgument("description", ""))
	assert.NoError(t, err)

	wgCreated.Wait()
	groupStr, err := createdMsg.GetArgument("group")
	assert.NoError(t, err)

	groupData := map[string]interface{}{}
	err = json.Unmarshal([]byte(groupStr), &groupData)
	assert.NoError(t, err)

	groupID, ok := groupData["id"].(string)
	assert.True(t, ok)

	wgJoined, _ := joinerReceiver.expectResponse("group_joined")

	err = websocket.JSON.Send(wsConnJoiner, message.New("join_group").WithArgument("group_id", groupID).WithArgument("user_id", joinerName))
	assert.NoError(t, err)

	wgJoined.Wait()

	wgStarted, _ := ownerReceiver.expectResponse("group_started")

	err = websocket.JSON.Send(wsConnOwner, message.New("start").WithArgument("user_id", ownerName).WithArgument("group_id", groupID))
	assert.NoError(t, err)

	wgStarted.Wait()

	// TODO check group updated response
}

func TestEOF(t *testing.T) {
	m := testManager(t)
	h := ws.NewHandler(m)

	mux := http.NewServeMux()
	mux.Handle("/ws", h.Handler())

	s := httptest.NewServer(mux)
	defer s.Close()

	wsConn, err := websocket.Dial(
		strings.Replace(s.URL+"/ws", "http://", "ws://", 1),
		"ws",
		"http://127.0.0.1",
	)
	assert.NoError(t, err)

	err = wsConn.Close()
	assert.NoError(t, err)
}

func TestMalformedJSON(t *testing.T) {
	m := testManager(t)
	h := ws.NewHandler(m)

	mux := http.NewServeMux()
	mux.Handle("/ws", h.Handler())

	s := httptest.NewServer(mux)
	defer s.Close()

	wsConn, err := dialWebsocket(s.URL)
	assert.NoError(t, err)

	r := setUpReceiver(t, wsConn)
	wg, errorMsg := r.expectResponse("error")

	err = websocket.Message.Send(wsConn, `{"foo:"bar"}`) // Missing '"'
	assert.NoError(t, err)

	wg.Wait()
	assert.Contains(t, errorMsg.Args["message"], "error receiving message: invalid character")
}

func TestNonExistingMessageType(t *testing.T) {
	m := testManager(t)
	h := ws.NewHandler(m)

	mux := http.NewServeMux()
	mux.Handle("/ws", h.Handler())

	s := httptest.NewServer(mux)
	defer s.Close()

	wsConn, err := dialWebsocket(s.URL)
	assert.NoError(t, err)

	r := setUpReceiver(t, wsConn)
	wg, errorMsg := r.expectResponse("error")

	err = websocket.JSON.Send(wsConn, message.New("invalid_type"))
	assert.NoError(t, err)

	wg.Wait()
	assert.Contains(t, errorMsg.Args["message"], "message type not defined")
}

func TestErrorConnecting(t *testing.T) {
	m := &managerMock{}
	h := ws.NewHandler(m)

	mux := http.NewServeMux()
	mux.Handle("/ws", h.Handler())

	s := httptest.NewServer(mux)
	defer s.Close()

	wsConn, err := dialWebsocket(s.URL)
	assert.NoError(t, err)

	r := setUpReceiver(t, wsConn)
	wg, errorMsg := r.expectResponse("error")

	err = websocket.JSON.Send(wsConn, message.New("connect").WithArgument("user_id", "test_user").WithArgument("user_name", "test_name").WithArgument("picture", ""))
	assert.NoError(t, err)

	wg.Wait()
	assert.Contains(t, errorMsg.Args["message"], "could not connect to manager")
}

type managerMock struct {
	manager.Manager
}

func (*managerMock) Connect(user.ID, string, string) (manager.Connection, []manager.UserGroup, error) {
	return nil, nil, fmt.Errorf("forced connection error")
}

func setUpReceiver(t *testing.T, wsConn *websocket.Conn) *websocketReceiver {
	r := &websocketReceiver{
		wsConn:   wsConn,
		messages: []message.Message{},
		expectations: map[string]struct {
			wg       *sync.WaitGroup
			response *message.Message
		}{},
	}

	r.listen(t, wsConn)

	return r
}

type websocketReceiver struct {
	wsConn       *websocket.Conn
	messages     []message.Message
	expectations map[string]struct {
		wg       *sync.WaitGroup
		response *message.Message
	}
}

func (r *websocketReceiver) listen(t *testing.T, wsConn *websocket.Conn) {
	msgCh := make(chan message.Message)

	go func() {
		for {
			var msg message.Message
			err := websocket.JSON.Receive(wsConn, &msg)
			assert.NoError(t, err)
			msgCh <- msg
		}
	}()

	go func() {
		for msg := range msgCh {
			found := false
			for msgType, expectation := range r.expectations {
				if msgType == msg.Type {
					*expectation.response = msg
					expectation.wg.Done()
					found = true
					delete(r.expectations, msgType)
				}
			}
			if !found {
				r.messages = append(r.messages, msg)
			}
		}
	}()
}

func (r *websocketReceiver) expectResponse(msgType string) (*sync.WaitGroup, *message.Message) {
	var wg sync.WaitGroup
	wg.Add(1)

	for _, msg := range r.messages {
		if msg.Type == msgType {
			defer wg.Done()
			return &wg, &msg
		}
	}

	response := &message.Message{}
	r.expectations[msgType] = struct {
		wg       *sync.WaitGroup
		response *message.Message
	}{
		wg:       &wg,
		response: response,
	}

	return &wg, response
}

func testManager(t *testing.T) manager.Manager {
	db, err := gorm.Open("sqlite3", ":memory:")
	assert.NoError(t, err)
	defer func() {
		_ = db.Close()
	}()

	return manager.New(db)
}

func dialWebsocket(url string) (*websocket.Conn, error) {
	return websocket.Dial(
		strings.Replace(url+"/ws", "http://", "ws://", 1),
		"ws",
		"http://127.0.0.1",
	)
}
