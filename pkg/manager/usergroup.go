package manager

import (
	"fmt"
	"time"

	"gitlab.com/rogerclotet/amicinvisible-server/pkg/group"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/user"
)

// A UserGroup is the representation of a group for a
// particular user
type UserGroup struct {
	ID          group.ID    `json:"id"`
	CreatedAt   time.Time   `json:"created_at"`
	UpdatedAt   time.Time   `json:"updated_at"`
	Name        string      `json:"name"`
	Description string      `json:"description"`
	Members     []user.User `json:"members"`
	OwnerID     string      `json:"owner_id"`
	Started     bool        `json:"started"`
	AssignedID  string      `json:"assigned_id,omitempty"`
}

func (m *manager) UserGroup(userID user.ID, groupID group.ID) (UserGroup, error) {
	g, err := m.group(groupID)
	if err != nil {
		return UserGroup{}, err
	}

	found := false
	var gu user.User
	for _, u := range g.Users {
		if u.ExternalID == userID {
			gu = u
			found = true
			break
		}
	}

	if !found {
		return UserGroup{}, fmt.Errorf("user not in group")
	}

	assignedID := ""
	assignment, err := g.Assignment(gu)
	if err == nil {
		assignedID = assignment.ExternalID
	}

	return UserGroup{
		ID:          g.ID,
		CreatedAt:   g.CreatedAt,
		UpdatedAt:   g.UpdatedAt,
		Name:        g.Name,
		Description: g.Description,
		Members:     g.Users,
		OwnerID:     g.OwnerID,
		Started:     g.Started(),
		AssignedID:  assignedID,
	}, nil
}
