package manager_test

import (
	"encoding/json"
	"sync"
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/group"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/manager"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/message"
)

func TestNew(t *testing.T) {
	m := testManager(t)

	assert.Implements(t, (*manager.Manager)(nil), m)
}

func TestGroup(t *testing.T) {
	m := testManager(t)

	ownerID := "owner"
	conn, _, err := m.Connect(ownerID, "test_name", "")
	assert.NoError(t, err)

	wg, groupStr := waitForResponseArg(t, "group_created", "group", conn.Responses())

	err = m.CreateGroup(ownerID, "test", "description")
	assert.NoError(t, err)

	userID := "test_user"
	_, _, err = m.Connect(userID, "test_name", "")
	assert.NoError(t, err)
	err = m.Disconnect(userID)
	assert.NoError(t, err)

	wg.Wait()
	groupID := groupIDFromCreateGroupResponse(t, groupStr)
	err = m.JoinGroup(userID, groupID)
	assert.NoError(t, err)

	err = m.LeaveGroup(userID, groupID)
	assert.NoError(t, err)
}

func TestCreateGroupWithNonExistingUser(t *testing.T) {
	m := testManager(t)

	err := m.CreateGroup("test", "group", "description")
	assert.Error(t, err)
}

func TestDeleteGroup(t *testing.T) {
	m := testManager(t)

	ownerID := "owner"

	conn, _, err := m.Connect(ownerID, "test_name", "")
	assert.NoError(t, err)

	wgCreated, groupStr := waitForResponseArg(t, "group_created", "group", conn.Responses())

	err = m.CreateGroup(ownerID, "group", "description")
	assert.NoError(t, err)

	wgCreated.Wait()
	groupID := groupIDFromCreateGroupResponse(t, groupStr)

	err = m.Disconnect(ownerID)
	assert.NoError(t, err)
	conn, _, err = m.Connect(ownerID, "test_name", "")
	assert.NoError(t, err)
	wgDeleted, deletedGroupID := waitForResponseArg(t, "group_deleted", "group_id", conn.Responses())

	err = m.DeleteGroup(ownerID, groupID)
	assert.NoError(t, err)

	wgDeleted.Wait()

	assert.Equal(t, groupID, *deletedGroupID)
}

func TestJoinNonExistingGroup(t *testing.T) {
	m := testManager(t)

	userID := "test_user"
	_, _, err := m.Connect(userID, "test_name", "")
	assert.NoError(t, err)

	err = m.JoinGroup(userID, uuid.NewV4().String())
	assert.Error(t, err)
}

func TestJoinGroupWithNonExistingUser(t *testing.T) {
	m := testManager(t)

	ownerID := "owner"
	conn, _, err := m.Connect(ownerID, "test_name", "")
	assert.NoError(t, err)
	wg, groupStr := waitForResponseArg(t, "group_created", "group", conn.Responses())

	err = m.CreateGroup(ownerID, "test_group", "description")
	assert.NoError(t, err)

	wg.Wait()

	groupID := groupIDFromCreateGroupResponse(t, groupStr)

	err = m.JoinGroup("test_user", groupID)
	assert.Error(t, err)
}

func TestLeaveNonExistingGroup(t *testing.T) {
	m := testManager(t)

	userID := "test_user"
	_, _, err := m.Connect(userID, "test_name", "")
	assert.NoError(t, err)

	err = m.LeaveGroup(userID, uuid.NewV4().String())
	assert.Error(t, err)
}

func TestLeaveGroupWithNonExistingUser(t *testing.T) {
	m := testManager(t)

	ownerID := "owner"
	conn, _, err := m.Connect(ownerID, "test_name", "")
	assert.NoError(t, err)
	wg, groupStr := waitForResponseArg(t, "group_created", "group", conn.Responses())

	err = m.CreateGroup(ownerID, "test_group", "description")
	assert.NoError(t, err)

	wg.Wait()

	groupID := groupIDFromCreateGroupResponse(t, groupStr)

	err = m.LeaveGroup("test_user", groupID)
	assert.Error(t, err)
}

func TestLeaveGroupWithUserThatIsNotAMember(t *testing.T) {
	m := testManager(t)

	ownerID := "owner"
	conn, _, err := m.Connect(ownerID, "test_name", "")
	assert.NoError(t, err)
	wg, groupStr := waitForResponseArg(t, "group_created", "group", conn.Responses())

	err = m.CreateGroup(ownerID, "test_group", "description")
	assert.NoError(t, err)

	wg.Wait()

	groupID := groupIDFromCreateGroupResponse(t, groupStr)

	userID := "test_user"
	_, _, err = m.Connect(userID, "test_name", "")
	assert.NoError(t, err)
	err = m.Disconnect(userID)
	assert.NoError(t, err)

	err = m.LeaveGroup(userID, groupID)
	assert.NoError(t, err)
}

func TestConnect(t *testing.T) {
	m := testManager(t)

	userID := "test_user"

	_, _, err := m.Connect(userID, "test_name", "")
	assert.NoError(t, err)
}

func TestConnectAlreadyConnected(t *testing.T) {
	m := testManager(t)

	userID := "test_user"

	_, _, err := m.Connect(userID, "test_name", "")
	assert.NoError(t, err)

	_, _, err = m.Connect(userID, "test_name", "")
	assert.Error(t, err)
}

func TestDisconnect(t *testing.T) {
	m := testManager(t)

	userID := "test_user"

	_, _, err := m.Connect(userID, "test_name", "")
	assert.NoError(t, err)

	err = m.Disconnect(userID)
	assert.NoError(t, err)
}

func TestDisconnectAndReconnect(t *testing.T) {
	m := testManager(t)

	userID := "test_user"

	c1, _, err := m.Connect(userID, "test_name", "")
	assert.NoError(t, err)

	err = m.Disconnect(userID)
	assert.NoError(t, err)

	c2, _, err := m.Connect(userID, "test_name", "")
	assert.NoError(t, err)

	assert.NotEqual(t, c1, c2)
}

func TestDisconnectWithNonExistentConnection(t *testing.T) {
	m := testManager(t)

	userID := "test_user"

	err := m.Disconnect(userID)
	assert.Error(t, err)
}

func TestProcessCreateGroup(t *testing.T) {
	m := testManager(t)

	userID := "test_user"
	conn, _, err := m.Connect(userID, "test_name", "")
	assert.NoError(t, err)
	wg, _ := waitForResponseArg(t, "group_created", "group", conn.Responses())

	err = m.Process(message.New("create_group").WithArgument("group_name", "test_group").WithArgument("description", "test_description").WithArgument("user_id", userID))
	assert.NoError(t, err)

	err = m.Process(message.New("create_group").WithArgument("group_name", "test_group").WithArgument("description", "test_description"))
	assert.Error(t, err)

	err = m.Process(message.New("create_group").WithArgument("group_name", "test_group").WithArgument("user_id", userID))
	assert.Error(t, err)

	err = m.Process(message.New("create_group").WithArgument("user_id", userID).WithArgument("description", "test_description"))
	assert.Error(t, err)

	wg.Wait()
}

func TestProcessDeleteGroup(t *testing.T) {
	m := testManager(t)

	userID := "test_owner"
	conn, _, err := m.Connect(userID, "test_name", "")
	assert.NoError(t, err)
	wg, groupStr := waitForResponseArg(t, "group_created", "group", conn.Responses())

	groupName := "test_group"
	err = m.CreateGroup(userID, groupName, "description")
	assert.NoError(t, err)

	wg.Wait()
	err = m.Disconnect(userID)
	assert.NoError(t, err)

	groupID := groupIDFromCreateGroupResponse(t, groupStr)

	conn, _, err = m.Connect(userID, "test_name", "")
	assert.NoError(t, err)
	wg, _ = waitForResponseArg(t, "group_deleted", "group_id", conn.Responses())

	err = m.Process(message.New("delete_group").WithArgument("group_id", groupID).WithArgument("user_id", userID))
	assert.NoError(t, err)

	wg.Wait()

	err = m.Process(message.New("delete_group").WithArgument("group_id", groupID))
	assert.Error(t, err)

	err = m.Process(message.New("delete_group").WithArgument("group_id", groupID).WithArgument("user_id", "another_user"))
	assert.Error(t, err)

	err = m.Process(message.New("delete_group").WithArgument("group_id", "invalid_group_id").WithArgument("user_id", userID))
	assert.Error(t, err)

	err = m.Process(message.New("delete_group").WithArgument("user_id", userID))
	assert.Error(t, err)
}

func TestProcessJoinGroup(t *testing.T) {
	m := testManager(t)

	ownerID := "test_owner"
	conn, _, err := m.Connect(ownerID, "test_name", "")
	assert.NoError(t, err)
	wg, groupStr := waitForResponseArg(t, "group_created", "group", conn.Responses())

	joinerID := "test_joiner"
	_, _, err = m.Connect(joinerID, "test_name", "")
	assert.NoError(t, err)
	err = m.Disconnect(joinerID)
	assert.NoError(t, err)

	groupName := "test_group"
	err = m.CreateGroup(ownerID, groupName, "description")
	assert.NoError(t, err)

	wg.Wait()

	groupID := groupIDFromCreateGroupResponse(t, groupStr)

	err = m.Process(message.New("join_group").WithArgument("group_id", groupID).WithArgument("user_id", joinerID))
	assert.NoError(t, err)

	err = m.Process(message.New("join_group").WithArgument("group_id", groupID))
	assert.Error(t, err)

	err = m.Process(message.New("join_group").WithArgument("group_id", groupID).WithArgument("user_id", "another_id"))
	assert.Error(t, err)

	err = m.Process(message.New("join_group").WithArgument("user_id", joinerID))
	assert.Error(t, err)

	err = m.Process(message.New("join_group").WithArgument("group_id", "12345").WithArgument("user_id", joinerID))
	assert.Error(t, err)
}

func TestProcessLeaveGroup(t *testing.T) {
	m := testManager(t)

	ownerID := "test_owner"
	conn, _, err := m.Connect(ownerID, "test_name", "")
	assert.NoError(t, err)
	wg, groupStr := waitForResponseArg(t, "group_created", "group", conn.Responses())

	joinerID := "test_joiner"
	_, _, err = m.Connect(joinerID, "test_name", "")
	assert.NoError(t, err)
	err = m.Disconnect(joinerID)
	assert.NoError(t, err)

	groupName := "test_group"
	err = m.CreateGroup(ownerID, groupName, "description")
	assert.NoError(t, err)

	wg.Wait()

	groupID := groupIDFromCreateGroupResponse(t, groupStr)

	err = m.JoinGroup(joinerID, groupID)
	assert.NoError(t, err)

	err = m.Process(message.New("leave_group").WithArgument("user_id", joinerID).WithArgument("group_id", groupID))
	assert.NoError(t, err)

	err = m.Process(message.New("leave_group").WithArgument("user_id", ownerID).WithArgument("group_id", groupID))
	assert.Error(t, err)

	err = m.Process(message.New("leave_group").WithArgument("group_id", groupID))
	assert.Error(t, err)

	err = m.Process(message.New("leave_group").WithArgument("user_id", joinerID))
	assert.Error(t, err)

	err = m.Process(message.New("leave_group").WithArgument("user_id", joinerID).WithArgument("group_id", "12345"))
	assert.Error(t, err)

	err = m.Process(message.New("leave_group").WithArgument("user_id", "other_user").WithArgument("group_id", groupID))
	assert.Error(t, err)
}

func TestProcessStart(t *testing.T) {
	m := testManager(t)

	ownerID := "test_owner"
	conn, _, err := m.Connect(ownerID, "test_name", "")
	assert.NoError(t, err)
	wg, groupStr := waitForResponseArg(t, "group_created", "group", conn.Responses())

	joinerID := "test_joiner"
	_, _, err = m.Connect(joinerID, "test_name", "")
	assert.NoError(t, err)
	err = m.Disconnect(joinerID)
	assert.NoError(t, err)

	groupName := "test_group"
	err = m.CreateGroup(ownerID, groupName, "description")
	assert.NoError(t, err)

	wg.Wait()

	groupID := groupIDFromCreateGroupResponse(t, groupStr)

	err = m.JoinGroup(joinerID, groupID)
	assert.NoError(t, err)

	err = m.Process(message.New("start").WithArgument("user_id", ownerID).WithArgument("group_id", groupID))
	assert.NoError(t, err)

	err = m.Process(message.New("start").WithArgument("user_id", joinerID).WithArgument("group_id", groupID))
	assert.Error(t, err)

	err = m.Process(message.New("start").WithArgument("user_id", "other_user").WithArgument("group_id", groupID))
	assert.Error(t, err)

	err = m.Process(message.New("start").WithArgument("group_id", groupID))
	assert.Error(t, err)

	err = m.Process(message.New("start").WithArgument("user_id", ownerID))
	assert.Error(t, err)

	err = m.Process(message.New("start").WithArgument("user_id", ownerID).WithArgument("group_id", "123456"))
	assert.Error(t, err)
}

func TestProcessNonExistingMessageType(t *testing.T) {
	m := testManager(t)

	err := m.Process(message.New("invalid_type"))
	assert.Error(t, err)
}

func testManager(t *testing.T) manager.Manager {
	db, err := gorm.Open("sqlite3", ":memory:")
	assert.NoError(t, err)
	defer func() {
		_ = db.Close()
	}()

	return manager.New(db)
}

func waitForResponseArg(t *testing.T, msgType, argName string, c <-chan message.Message) (*sync.WaitGroup, *string) {
	var (
		wg     sync.WaitGroup
		result string
	)
	wg.Add(1)
	go func() {
		for msg := range c {
			if msg.Type == msgType {
				var err error
				result, err = msg.GetArgument(argName)
				assert.NoError(t, err)
				wg.Done()
			}
		}
	}()
	return &wg, &result
}

func groupIDFromCreateGroupResponse(t *testing.T, groupStr *string) group.ID {
	var g map[string]interface{}
	err := json.Unmarshal([]byte(*groupStr), &g)
	assert.NoError(t, err)

	groupID, ok := g["id"].(string)
	assert.True(t, ok)

	return groupID
}
