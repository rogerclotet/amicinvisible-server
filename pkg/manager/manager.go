package manager

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"github.com/jinzhu/gorm"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/group"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/message"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/user"
)

// Manager manages groups in a centralized way
type Manager interface {
	// Connection
	Connect(userID user.ID, userName, picture string) (Connection, []UserGroup, error)
	Disconnect(user.ID) error

	// Groups
	Group(groupID group.ID) (group.Group, error)
	UserGroup(user.ID, group.ID) (UserGroup, error)
	CreateGroup(userID user.ID, groupName, description string) error
	DeleteGroup(user.ID, group.ID) error
	JoinGroup(user.ID, group.ID) error
	LeaveGroup(user.ID, group.ID) error
	Start(user.ID, group.ID) error

	// Processor
	Process(message.Message) error
}

// New creates a new manager
func New(db *gorm.DB) Manager {
	db.AutoMigrate(&user.User{}, &group.Group{})

	var usersSlice []user.User
	db.Find(&usersSlice)
	users := make(map[string]user.User)
	for _, u := range usersSlice {
		users[u.ExternalID] = u
	}

	var groupsSlice []group.Group
	db.Preload("Users").Find(&groupsSlice)
	groups := make(map[group.ID]group.Group)
	for _, g := range groupsSlice {
		groups[g.ID] = g
	}

	return &manager{
		db:          db,
		connections: make(map[string]Connection),
		users:       users,
		groups:      groups,
	}
}

type manager struct {
	db *gorm.DB

	connections map[string]Connection
	cMutex      sync.RWMutex

	users  map[string]user.User
	uMutex sync.RWMutex

	groups map[group.ID]group.Group
	gMutex sync.RWMutex
}

func (m *manager) Connect(userID, userName, picture string) (Connection, []UserGroup, error) {
	m.cMutex.Lock()
	defer m.cMutex.Unlock()

	if _, ok := m.connections[userID]; ok {
		return nil, nil, fmt.Errorf("connection already active for %s", userID)
	}

	c := NewConnection()
	m.connections[userID] = c

	u := user.New(userID, userName, picture)
	m.db.Save(&u) // Saving even if user already exists just in case they changed name/picture
	m.users[userID] = u

	return c, m.userGroups(userID), nil
}

func (m *manager) Disconnect(userID user.ID) error {
	m.cMutex.Lock()
	defer m.cMutex.Unlock()

	c, err := m.connection(userID)
	if err != nil {
		return err
	}

	c.Disconnect()
	delete(m.connections, userID)

	return nil
}

func (m *manager) Group(groupID group.ID) (group.Group, error) {
	if g, ok := m.groups[groupID]; ok {
		return g, nil
	}

	return group.Group{}, fmt.Errorf("group %s not found", groupID)
}

func (m *manager) CreateGroup(userID user.ID, groupName, description string) error {
	m.gMutex.Lock()
	defer m.gMutex.Unlock()

	u, err := m.user(userID)
	if err != nil {
		return err
	}

	g := group.New(groupName, description, u)
	m.db.Create(&g)
	m.groups[g.ID] = g

	groupData, err := json.Marshal(g)
	if err == nil {
		m.respond(userID, message.New("group_created").WithArgument("group", string(groupData)))
	} else {
		log.Printf("could not encode groups data: %s", err)
	}

	return nil
}

func (m *manager) DeleteGroup(userID user.ID, groupID group.ID) error {
	m.gMutex.Lock()
	defer m.gMutex.Unlock()

	g, ok := m.groups[groupID]
	if !ok {
		return fmt.Errorf("group not found")
	}

	if userID != g.OwnerID {
		return fmt.Errorf("trying to delete group not being owner")
	}

	m.broadcast(g, message.New("group_deleted").WithArgument("group_id", groupID))

	m.db.Delete(g)
	delete(m.groups, groupID)

	return nil
}

func (m *manager) JoinGroup(userID user.ID, gID group.ID) error {
	m.uMutex.RLock()
	defer m.uMutex.RUnlock()
	u, err := m.user(userID)
	if err != nil {
		return err
	}

	m.gMutex.Lock()
	defer m.gMutex.Unlock()
	g, err := m.group(gID)
	if err != nil {
		return err
	}

	err = g.Join(u)
	if err != nil {
		return fmt.Errorf("could not join: %s", err)
	}

	m.db.Save(&g)
	m.groups[gID] = g

	m.respond(userID, message.New("group_joined"))

	return m.broadcastGroupUpdate(g)
}

func (m *manager) LeaveGroup(userID user.ID, groupID group.ID) error {
	m.uMutex.RLock()
	defer m.uMutex.RUnlock()
	u, err := m.user(userID)
	if err != nil {
		return err
	}

	m.gMutex.RLock()
	defer m.gMutex.RUnlock()
	g, err := m.group(groupID)
	if err != nil {
		return err
	}

	err = g.Leave(u)
	if err != nil {
		return fmt.Errorf("could not leave group: %s", err)
	}

	groupData, err := json.Marshal(g)
	if err != nil {
		return fmt.Errorf("could not encode group: %v", err)
	}

	m.respond(userID, message.New("group_left").WithArgument("group", string(groupData)))

	return m.broadcastGroupUpdate(g)
}

func (m *manager) Start(userID user.ID, groupID group.ID) error {
	m.uMutex.RLock()
	defer m.uMutex.RUnlock()
	u, err := m.user(userID)
	if err != nil {
		return err
	}

	m.gMutex.Lock()
	defer m.gMutex.Unlock()
	g, err := m.group(groupID)
	if err != nil {
		return err
	}

	err = g.Start(u)
	if err != nil {
		return fmt.Errorf("could not start group: %s", err)
	}

	m.db.Save(&g)
	m.groups[groupID] = g

	m.respond(userID, message.New("group_started"))

	return m.broadcastGroupUpdate(g)
}

func (m *manager) Process(msg message.Message) error {
	switch msg.Type {
	case "create_group":
		groupName, err := msg.GetArgument("group_name")
		if err != nil {
			return err
		}
		description, err := msg.GetArgument("description")
		if err != nil {
			return err
		}
		userID, err := msg.GetArgument("user_id")
		if err != nil {
			return err
		}
		return m.CreateGroup(userID, groupName, description)
	case "delete_group":
		groupID, err := msg.GetArgument("group_id")
		if err != nil {
			return err
		}
		userID, err := msg.GetArgument("user_id")
		if err != nil {
			return err
		}
		return m.DeleteGroup(userID, groupID)
	case "join_group":
		groupID, err := msg.GetArgument("group_id")
		if err != nil {
			return err
		}
		userID, err := msg.GetArgument("user_id")
		if err != nil {
			return err
		}
		return m.JoinGroup(userID, groupID)
	case "leave_group":
		userID, err := msg.GetArgument("user_id")
		if err != nil {
			return err
		}
		groupID, err := msg.GetArgument("group_id")
		if err != nil {
			return err
		}
		return m.LeaveGroup(userID, groupID)
	case "start":
		userID, err := msg.GetArgument("user_id")
		if err != nil {
			return err
		}
		groupID, err := msg.GetArgument("group_id")
		if err != nil {
			return err
		}
		return m.Start(userID, groupID)
	}

	return fmt.Errorf("message type not defined: %s", msg.Type)
}

func (m *manager) connection(userID user.ID) (Connection, error) {
	c, ok := m.connections[userID]
	if !ok {
		return nil, fmt.Errorf("connection not found for user: %s", userID)
	}

	return c, nil
}

func (m *manager) user(id user.ID) (user.User, error) {
	u, ok := m.users[id]
	if !ok {
		return user.User{}, fmt.Errorf("user not found: %s", id)
	}

	return u, nil
}

func (m *manager) group(groupID group.ID) (group.Group, error) {
	g, ok := m.groups[groupID]
	if !ok {
		return group.Group{}, fmt.Errorf("group not found: %s", groupID)
	}

	return g, nil
}

func (m *manager) respond(userName user.ID, msg message.Message) {
	if c, err := m.connection(userName); err == nil {
		c.Send(msg)
	}
}

func (m *manager) broadcast(g group.Group, msg message.Message) {
	for _, u := range g.Users {
		if c, err := m.connection(u.ExternalID); err == nil {
			c.Send(msg)
		}
	}
}

func (m *manager) broadcastGroupUpdate(g group.Group) error {
	for _, u := range g.Users {
		if c, err := m.connection(u.ExternalID); err == nil {
			ug, err := m.UserGroup(u.ExternalID, g.ID)
			if err != nil {
				return fmt.Errorf("could not get user group: %v", err)
			}

			groupData, err := json.Marshal(ug)
			if err != nil {
				return fmt.Errorf("could not encode user group: %v", err)
			}

			msg := message.New("group_updated").WithArgument("group", string(groupData))
			c.Send(msg)
		}
	}

	return nil
}

// TODO implement this in a better way
func (m *manager) userGroups(userID user.ID) []UserGroup {
	groups := []UserGroup{}
	for _, g := range m.groups {
		ug, err := m.UserGroup(userID, g.ID)
		if err == nil {
			groups = append(groups, ug)
		} else {
			log.Printf("could not load user group: %v", err)
		}
	}
	return groups
}
