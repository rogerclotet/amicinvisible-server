package manager_test

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/manager"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/message"
)

func TestNewConnection(t *testing.T) {
	c := manager.NewConnection()

	assert.Implements(t, (*manager.Connection)(nil), c)
}

func TestSend(t *testing.T) {
	c := manager.NewConnection()

	r := c.Responses()

	wg := sync.WaitGroup{}
	wg.Add(1)

	var recvMsg message.Message
	go func(msg *message.Message) {
		recvMsg = <-r
		wg.Done()
	}(&recvMsg)

	msg := message.Message{Type: "test"}
	c.Send(msg)

	wg.Wait()
	assert.Equal(t, msg, recvMsg)
}
