package manager

import "gitlab.com/rogerclotet/amicinvisible-server/pkg/message"

// A Connection represents a communications channel
// between the server and a user
type Connection interface {
	Responses() <-chan message.Message
	Disconnect()

	Send(message.Message)
}

// NewConnection creates a new Connection
func NewConnection() Connection {
	return &connection{
		responses: make(chan message.Message),
	}
}

type connection struct {
	responses chan message.Message
}

func (c *connection) Responses() <-chan message.Message {
	return c.responses
}

func (c *connection) Disconnect() {
	close(c.responses)
}

func (c *connection) Send(msg message.Message) {
	c.responses <- msg
}
