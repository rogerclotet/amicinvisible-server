package group

import (
	"fmt"
	"math/rand"
	"time"

	"encoding/json"

	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/user"
)

// ID identifies uniquely a Group in the system
type ID = string

// New creates a new Group
func New(name, description string, owner user.User) Group {
	return Group{
		Name:        name,
		Description: description,
		Users:       []user.User{owner},
		OwnerID:     owner.ExternalID,
		assignments: []string{},
	}
}

// A Group represents a Group of users who will participate
// in the secret santa
type Group struct {
	ID        ID         `gorm:"primary_key" json:"id"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `sql:"index" json:"-"`

	Name        string      `json:"name"`
	Description string      `json:"description"`
	Users       []user.User `gorm:"many2many:group_users;" json:"members"`
	OwnerID     string      `json:"owner_id"`

	assignments           []string
	SerializedAssignments string `json:"-"`
}

// BeforeCreate is a gorm callback called before creating a new Group
func (g *Group) BeforeCreate(scope *gorm.Scope) error {
	return scope.SetColumn("ID", uuid.NewV4().String())
}

// AfterFind is a gorm callback called after a Group is queried
func (g *Group) AfterFind() error {
	if g.SerializedAssignments == "" {
		return nil
	}

	err := json.Unmarshal([]byte(g.SerializedAssignments), &g.assignments)
	if err != nil {
		return fmt.Errorf("could not unmarshal assignments after find: %v", err)
	}

	return nil
}

// Join adds the user to the group
func (g *Group) Join(u user.User) error {
	for _, member := range g.Users {
		if member.ExternalID == u.ExternalID {
			return nil // Already in Group
		}
	}

	g.Users = append(g.Users, u)

	return nil
}

// Leave removes the user from the group
func (g *Group) Leave(u user.User) error {
	if u.ExternalID == g.OwnerID {
		return fmt.Errorf("owner can not leave a Group")
	}

	for i, member := range g.Users {
		if member.ExternalID == u.ExternalID {
			g.Users = append(g.Users[:i], g.Users[i+1:]...)
			return nil
		}
	}

	return nil // Not in Group, ignoring it for now
}

// Start assigns a friend to each user and marks the group as started
func (g *Group) Start(u user.User) error {
	if u.ExternalID != g.OwnerID {
		return fmt.Errorf("user starting group is not the owner")
	}

	if len(g.assignments) > 0 {
		return fmt.Errorf("already started")
	}

	if len(g.Users) < 2 {
		return fmt.Errorf("tried to start with less than 2 members")
	}

	g.assignments = make([]string, len(g.Users))

	shuffledMembers := make([]int, len(g.Users))
	perm := rand.Perm(len(g.Users))
	for i, v := range perm {
		shuffledMembers[v] = i
	}

	for i := 0; i < len(shuffledMembers)-1; i++ {
		assigned := g.Users[shuffledMembers[i+1]]
		g.assignments[shuffledMembers[i]] = assigned.ExternalID
	}
	g.assignments[shuffledMembers[len(shuffledMembers)-1]] = g.Users[shuffledMembers[0]].ExternalID

	// Save serialized assignments to be able to store it in database
	serializedBytes, err := json.Marshal(g.assignments)
	if err != nil {
		return fmt.Errorf("could not marshal assignments before saving: %v", err)
	}

	g.SerializedAssignments = string(serializedBytes)

	return nil
}

// Assignment returns the friend for a given user
func (g *Group) Assignment(u user.User) (user.User, error) {
	if !g.Started() {
		return user.User{}, fmt.Errorf("group not started yet")
	}

	pos := -1
	for k, member := range g.Users {
		if member.ExternalID == u.ExternalID {
			pos = k
			break
		}
	}

	if pos == -1 {
		return user.User{}, fmt.Errorf("user %s not in group", u.ExternalID)
	}

	id := g.assignments[pos]
	for _, member := range g.Users {
		if member.ExternalID == id {
			return member, nil
		}
	}

	return user.User{}, fmt.Errorf("assigned user %s not found", id)
}

// Started returns whether the group is started or not
func (g *Group) Started() bool {
	return len(g.assignments) > 0
}
