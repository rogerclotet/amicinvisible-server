package group_test

import (
	"math/rand"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/group"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/user"
)

func TestNew(t *testing.T) {
	u := user.New("foo", "test_name", "")
	g := group.New("test", "test_description", u)

	assert.IsType(t, group.Group{}, g)
}

func TestName(t *testing.T) {
	name := strconv.Itoa(rand.Int())

	u := user.New("foo", "test_name", "")
	g := group.New(name, "test_description", u)

	assert.Equal(t, name, g.Name)
}

func TestDescription(t *testing.T) {
	description := strconv.Itoa(rand.Int())

	u := user.New("foo", "test_name", "")
	g := group.New("bar", description, u)

	assert.Equal(t, description, g.Description)
}

func TestOwnerIsAMember(t *testing.T) {
	u := user.New("foo", "test_name", "")
	g := group.New("test", "test_description", u)

	assert.Contains(t, g.Users, u)
}

func TestJoin(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	u := user.New("joiner", "test_name", "")
	err := g.Join(u)
	assert.NoError(t, err)

	assert.Contains(t, g.Users, u)
}

func TestJoinWithAlreadyMember(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	u := user.New("joiner", "test_name", "")
	err := g.Join(u)
	assert.NoError(t, err)

	err = g.Join(u)
	assert.NoError(t, err)

	assert.Contains(t, g.Users, u)
}

func TestLeave(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	u := user.New("joiner", "test_name", "")
	err := g.Join(u)

	assert.NoError(t, err)

	err = g.Leave(u)
	assert.NoError(t, err)
	assert.NotContains(t, g.Users, u)
}

func TestLeaveWithNonMember(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	u := user.New("non_member", "test_name", "")

	err := g.Leave(u)
	assert.NoError(t, err)
	assert.NotContains(t, g.Users, u)
}

func TestOwnerCanNotLeave(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	err := g.Leave(owner)

	assert.Error(t, err)
}

func TestStartWithTwoUsers(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	secondUser := user.New("second", "test_name", "")
	err := g.Join(secondUser)
	assert.NoError(t, err)

	err = g.Start(owner)
	assert.NoError(t, err)

	assignedToOwner, err := g.Assignment(owner)
	assert.NoError(t, err)
	assert.Equal(t, secondUser, assignedToOwner)

	assignedToSecond, err := g.Assignment(secondUser)
	assert.NoError(t, err)
	assert.Equal(t, owner, assignedToSecond)
}

func TestStartingGroupWithMoreUsers(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	var users []user.User
	for i := 0; i < 9; i++ {
		u := user.New(strconv.Itoa(i), "test_name", "")
		err := g.Join(u)
		assert.NoError(t, err)

		users = append(users, u)
	}

	err := g.Start(owner)
	assert.NoError(t, err)

	alreadyAssigned := make(map[user.User]bool)
	assignment, err := g.Assignment(owner)
	assert.NoError(t, err)
	alreadyAssigned[assignment] = true
	for _, u := range users {
		assignment, err := g.Assignment(u)
		assert.NoError(t, err)

		_, ok := alreadyAssigned[assignment]
		assert.False(t, ok)

		alreadyAssigned[assignment] = true
	}
}

func TestStartingFromNonOwner(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	secondUser := user.New("second", "test_name", "")
	err := g.Join(secondUser)
	assert.NoError(t, err)

	err = g.Start(secondUser)
	assert.Error(t, err)
	assert.Equal(t, "user starting group is not the owner", err.Error())
}

func TestStartingWithOneUser(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	err := g.Start(owner)
	assert.Error(t, err)
	assert.Equal(t, "tried to start with less than 2 members", err.Error())
}

func TestStartingAlreadyStartedGroup(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	secondUser := user.New("second", "test_name", "")
	err := g.Join(secondUser)
	assert.NoError(t, err)

	err = g.Start(owner)
	assert.NoError(t, err)

	err = g.Start(owner)
	assert.Error(t, err)
	assert.Equal(t, "already started", err.Error())
}

func TestAssignmentWhenNotStarted(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	_, err := g.Assignment(owner)
	assert.Error(t, err)
	assert.Equal(t, "group not started yet", err.Error())
}

func TestAssignmentIfUserNotInGroup(t *testing.T) {
	owner := user.New("owner", "test_name", "")
	g := group.New("test", "test_description", owner)

	secondUser := user.New("second", "test_name", "")
	err := g.Join(secondUser)
	assert.NoError(t, err)

	err = g.Start(owner)
	assert.NoError(t, err)

	_, err = g.Assignment(user.New("third", "test_name", ""))
	assert.Error(t, err)
}
