package user_test

import (
	"math/rand"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/user"
)

func TestNew(t *testing.T) {
	u := user.New("test", "test_name", "")

	assert.IsType(t, user.User{}, u)
}

func TestExternalID(t *testing.T) {
	id := strconv.Itoa(rand.Int())
	u := user.New(id, "test_name", "")

	assert.Equal(t, id, u.ExternalID)
}

func TestName(t *testing.T) {
	name := strconv.Itoa(rand.Int())
	u := user.New("test_id", name, "")

	assert.Equal(t, name, u.Name)
}
