package user

import (
	"github.com/jinzhu/gorm"
)

// ID represents a user ID
type ID = string

// New creates a new User
func New(id, name, picture string) User {
	return User{
		ExternalID: id,
		Name:       name,
		Picture:    picture,
	}
}

// A User is the representation of a user in the application
type User struct {
	gorm.Model

	ExternalID string `json:"id"` // TODO deal with internal/external IDs somehow
	Name       string `json:"name"`
	Picture    string `json:"picture"`
}
