package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"flag"

	"github.com/husobee/vestigo"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/rs/cors"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/manager"
	"gitlab.com/rogerclotet/amicinvisible-server/pkg/websocket"
)

func main() {
	var addr, mysqlAddr string

	port := os.Getenv("PORT")
	if port != "" {
		addr = ":" + port
	}

	mysqlAddr = os.Getenv("MYSQL_ADDR")

	flag.StringVar(&addr, "addr", addr, "http service address")
	flag.StringVar(&mysqlAddr, "mysql_addr", mysqlAddr, "mysql connection address")

	flag.Parse()

	log.Fatal(StartServer(addr, mysqlAddr))
}

// StartServer starts a new server in the given address
func StartServer(addr, mysqlAddr string) error {
	mysqlConnection := fmt.Sprintf("%s?charset=utf8&parseTime=True&loc=Local", mysqlAddr)
	db, err := gorm.Open("mysql", mysqlConnection)
	if err != nil {
		log.Fatalf("could not connect to database (%s): %v", mysqlConnection, err)
	}
	defer func() {
		err := db.Close()
		if err != nil {
			log.Printf("error closing db: %v", err)
		}
	}()

	m := manager.New(db)
	ws := websocket.NewHandler(m)

	router := vestigo.NewRouter()
	router.Handle("/ws", ws.Handler())
	router.Get("/group/:groupID", groupInfoHandler(m))
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("status check ok")
		w.WriteHeader(http.StatusOK)
	})

	log.Printf("listening to %s", addr)

	handler := cors.Default().Handler(router)
	return http.ListenAndServe(addr, handler)
}

func groupInfoHandler(m manager.Manager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		groupID := vestigo.Param(r, "groupID")

		g, err := m.Group(groupID)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "Group not found")
			return
		}

		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(g)
		if err != nil {
			log.Printf("error encoding groups response: %v", err)
		}
	}
}
