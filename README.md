# Amic Invisible

This is the backend for a simple Secret Santa application.

You can see the code coverage
[here](https://rogerclotet.gitlab.io/amicinvisible-server).

## Run locally

You just need to use [dep](http://github.com/golang/dep/cmd/dep)
to get the needed dependencies and build or run main.go

Get dependencies:
```bash
go get -u github.com/golang/dep/cmd/dep
dep ensure
```

Run:
```bash
go run main.go
# OR
go build -o amicinvisible-server main.go
./amicinvisible-server
```
