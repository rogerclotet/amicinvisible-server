FROM golang:1.9

ADD . /go/src/gitlab.com/rogerclotet/amicinvisible-server
WORKDIR /go/src/gitlab.com/rogerclotet/amicinvisible-server

RUN go get github.com/golang/dep/cmd/dep
RUN dep ensure
RUN go build -o amicinvisible

CMD ["./amicinvisible"]

EXPOSE 80
